package com.example.macintosh.congratulatoinscard;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "com.example.congratulationscard.MESSAGE";
    public static final String EXTRA_SENDER = "com.example.congratulationscard.SENDER";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void sendMessage(View view){
        //creat intent instance
        Intent intent = new Intent(this,DisplayMessageActivity.class);

        EditText edit_textmessage = (EditText) findViewById(R.id.edit_textmessage);
        EditText editText_yourname = (EditText) findViewById(R.id.edit_text_name);

        String message = edit_textmessage.getText().toString();
        String yourNameString = editText_yourname.getText().toString();

        //explicit Intent
        intent.putExtra(EXTRA_MESSAGE,message);
        intent.putExtra(EXTRA_SENDER,yourNameString);

        startActivity(intent);
    }
}
