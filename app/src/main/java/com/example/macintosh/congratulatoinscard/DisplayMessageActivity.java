package com.example.macintosh.congratulatoinscard;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DisplayMessageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);

        Intent intent = getIntent();

        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        String sendername = intent.getStringExtra(MainActivity.EXTRA_SENDER);

        TextView messageTextView = (TextView) findViewById(R.id.DisplayMessageTextView);
        TextView senderTextView = (TextView) findViewById(R.id.displaySenderName);

        messageTextView.setText(message);
        senderTextView.append(sendername);
    }
}
